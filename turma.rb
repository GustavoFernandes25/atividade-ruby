

class Turma 
    attr_accessor :nome, :horario, :inscritos, :inscricao_aberta, :dias_semana
    def initialize(nome, horario, dias_semana, inscritos)
        @nome = nome
        @horario = horario
        @dias_semana = []
        @inscritos = []
        @inscricao_aberta = false
    end
    def abrir_inscricao
        print "Abrir inscrição? yes ou not"
        valor = gets
        if valor == "yes"
            @inscricao_aberta = true
        else
            @inscricao_aberta = false
        end
    end

end